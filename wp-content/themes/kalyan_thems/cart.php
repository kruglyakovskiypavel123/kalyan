<?php
/**
 * Template name: Cart page
 */

get_header();
?>
    <main class="main">
        <section class="s-cart">
            <div class="section-bold-title">Корзина</div>
            <div class="alert-price">Минимальный заказ для оформления не менее 500 рублей</div>

            <div class="cart-table">
                <div class="cart-table__item">
                    <div class="cart-table__image">
                        <img src="<?php bloginfo('template_directory') ?>/assets/img/cart/1.png" alt="">
                    </div>
                    <div class="cart-table__name">
                        <a href="single-page.html">Pharaon Premium M1007</a>
                    </div>
                    <div class="cart-table__price">
                        <div class="cart-product-price">3780 руб</div>
                        <span class="cart-table__multiply">x</span>
                        <div class="product-counter">
                            <button type="button" class="product-counter__add">-</button>
                            <input class="product-counter__number" value="3" type="text">
                            <button type="button" class="product-counter__remove">+</button>
                        </div>
                        <span class="cart-table__equally"></span>
                        <div class="cart-total-price">11340 руб</div>
                    </div>
                    <div class="cart-table__delete">
                        <button type="button" class="btn-delete"><i class="i-delete"></i></button>
                    </div>
                </div>

                <div class="cart-table__item">
                    <div class="cart-table__image">
                        <img src="<?php bloginfo('template_directory') ?>/assets/img/cart/2.png" alt="">
                    </div>
                    <div class="cart-table__name">
                        <a href="single-page.html">Must Have Mango Sling</a>
                    </div>
                    <div class="cart-table__price">
                        <div class="cart-product-price">730 руб</div>
                        <span class="cart-table__multiply">x</span>
                        <div class="product-counter">
                            <button type="button" class="product-counter__add">-</button>
                            <input class="product-counter__number" value="44" type="text">
                            <button type="button" class="product-counter__remove">+</button>
                        </div>
                        <span class="cart-table__equally"></span>
                        <div class="cart-total-price">32120 руб</div>
                    </div>
                    <div class="cart-table__delete">
                        <button type="button" class="btn-delete"><i class="i-delete"></i></button>
                    </div>
                </div>

                <div class="cart-table__item">
                    <div class="cart-table__image">
                        <img src="<?php bloginfo('template_directory') ?>/assets/img/cart/3.png" alt="">
                    </div>
                    <div class="cart-table__name">
                        <a href="single-page.html">Shaman 96 шт.</a>
                    </div>
                    <div class="cart-table__price">
                        <div class="cart-product-price">300 руб</div>
                        <span class="cart-table__multiply">x</span>
                        <div class="product-counter">
                            <button type="button" class="product-counter__add">-</button>
                            <input class="product-counter__number" value="10" type="text">
                            <button type="button" class="product-counter__remove">+</button>
                        </div>
                        <span class="cart-table__equally"></span>
                        <div class="cart-total-price">3000 руб</div>
                    </div>
                    <div class="cart-table__delete">
                        <button type="button" class="btn-delete"><i class="i-delete"></i></button>
                    </div>
                </div>
            </div>
            <div class="cart-total">
                <div class="cart-total__price">Итого 46460 руб</div>
                <div class="coupon">
                    <label for="coupon_id" class="coupon-label">Купон на скидку</label>
                    <input type="text" id="coupon_id" class="input coupon-input">
                    <button type="button" class="btn btn-ok btn-regular btn-orange circleflash">
                        <span class="btn__text">Ок</span>
                        <span class="circle"></span>
                    </button>
                </div>
                <div class="clear-cart">
                    <button type="submit" class="clear-cart-submit">Очистить корзину</button>
                </div>
                <div class="submit-cart">
                    <button type="button" class="btn btn-regular btn-orange circleflash">
                        <span class="circle"></span>Оформить
                    </button>
                </div>
            </div>
        </section>
    </main>
    <!-- /.main -->
<?php
get_footer();