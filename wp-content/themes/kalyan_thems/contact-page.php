<?php
/**
 * Template name: Contact page
 */

get_header();
?>
    <main class="main">
        <section class="breadcrumb">
            <ul class="breadcrumbs">
                <li class="breadcrumbs__item"><a href="" class="breadcrumbs__link">Главная</a></li>
                <li class="breadcrumbs__item"><a href="" class="breadcrumbs__link active">Контакты</a></li>
            </ul>
        </section>
        <!-- /.breadcrumb -->

        <section class="s-contacts">
            <div class="contact-title">Контакты</div>
            <div class="contacts-grid">
                <ul class="contacts-list">
                    <li>
                        <span class="contacts-list__title">Адрес:</span>
                        <span class="contacts-list__text">
                            109559, Россия, г. Москва, ул. Тихорецкий бульвар д. 1
                        </span>
                    </li>

                    <li>
                        <span class="contacts-list__title">График работы:</span>
                        <span class="contacts-list__text">
                            Понедельник-пятница, 9:00-18:00, Сб-Вс выходной <br>
                            Заказы через сайт принимаются круглосуточно, обрабатываются в рабочее время
                        </span>
                    </li>

                    <li>
                        <span class="contacts-list__title">Телефон:</span>
                        <span class="contacts-list__text">+7 926 918-54-75</span>
                    </li>

                    <li>
                        <span class="contacts-list__title">E-mail:</span>
                        <span class="contacts-list__text">info@intoco.ru</span>
                    </li>
                </ul>
                <div class="map">
                    <img src="<?php bloginfo('template_directory') ?>/assets/img/map.png" alt="">
                </div>
            </div>
        </section>
    </main>
    <!-- /.main -->
<?php
get_footer();