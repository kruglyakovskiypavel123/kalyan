<?php
/**
 * Template name: Checkout Step two page
 */

get_header();
?>
    <main class="main">
        <section class="s-checkout">
            <div class="section-bold-title">Оформление заказа</div>
            <div class="checkout-steps">
                <div class="checkout-steps__title disable">
                    <span class="color-orange">Шаг 1.</span> Контактные данные
                </div>

                <div class="checkout-steps__title">
                    <span class="color-orange">Шаг 2.</span> Способ доставки
                </div>

                <div class="checkout-steps__title disable">
                    <span class="color-orange">Шаг 3.</span> Подтверждение и оплата
                </div>
            </div>
            <div class="checkout-block">
                <form action="">
                    <div class="checkout-block__title">Самовывоз из магазина в Москве</div>
                    <div class="form-group mb-15">
                        <input type="checkbox" id="street1" hidden>
                        <label for="street1" class="checkbox checkout__checkbox">ул. Тихорецкий бульвар д. 1</label>
                    </div>
                    <div class="form-group mb-35">
                        <input type="checkbox" id="street2" hidden>
                        <label for="street2" class="checkbox checkout__checkbox">ул. Петровский п-р д. 38</label>
                    </div>

                    <div class="checkout-block__title">СДЭК - экономичная доставка в города России и СНГ</div>
                    <div class="form-group mb-35">
                        <input type="checkbox" id="delivery" hidden>
                        <label for="delivery" class="checkbox checkout__checkbox">Доставка СДЭК, от - 250 р.</label>
                    </div>

                    <div class="checkout-block__title">Иные способы доставки</div>
                    <div class="form-group mb-15">
                        <input type="checkbox" id="otherDelivery" hidden>
                        <label for="otherDelivery" class="checkbox checkout__checkbox">ТК на Ваш выбор, от 250 р - Стоимость доставки уточнит наш менеджер.</label>
                    </div>
                    <div class="form-group mb-40">
                        <input type="checkbox" id="otherDelivery2" hidden>
                        <label for="otherDelivery2" class="checkbox checkout__checkbox">Курьерская служба EMS - Стоимость доставки рассчитает менеджер!</label>
                    </div>

                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-100 btn-regular btn-orange circleflash">
                            <span class="btn__text">
                                <a href="/checkout-step-three-page">Дальше</a>
                            </span>
                            <span class="circle"></span>
                        </button>
                    </div>
                </form>
            </div>
        </section>
    </main>
    <!-- /.main -->
<?php
get_footer();