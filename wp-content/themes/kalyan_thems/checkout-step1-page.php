<?php
/**
 * Template name: Checkout Step one page
 */

get_header();
?>
    <main class="main">
        <section class="s-checkout">
            <div class="section-bold-title">Оформление заказа</div>
            <div class="checkout-steps">
                <div class="checkout-steps__title">
                    <span class="color-orange">Шаг 1.</span> Контактные данные
                </div>

                <div class="checkout-steps__title disable">
                    <span class="color-orange">Шаг 2.</span> Способ доставки
                </div>

                <div class="checkout-steps__title disable">
                    <span class="color-orange">Шаг 3.</span> Подтверждение и оплата
                </div>
            </div>
            <div class="checkout-block">
                <form action="">
                    <div class="form-group mb-20">
                        <label class="label" for="user_fullname">ФИО получателя</label>
                        <input class="input input-20" type="text" id="user_fullname" value="Иванов Иван Иванович">
                    </div>

                    <div class="form-group mb-20">
                        <label class="label" for="user_address">Полный адрес доставки, Индекс, все через запятую</label>
                        <input class="input input-20" type="text" id="user_address" value="Россия, г. Москва, Новый Арбат 178, 3 корпус, кв 44, индекс 768493">
                    </div>

                    <div class="form-group mb-20">
                        <label class="label" for="user_phone">Номер телефона получателя</label>
                        <input class="input input-20" type="text" id="user_phone" value="+7 999 879-87-32">
                    </div>

                    <div class="form-group mb-20">
                        <label class="label" for="user_email">E-mail получателя</label>
                        <input class="input input-20" type="text" id="user_email" value="ivanov-i-i@yandex.ru">
                    </div>

                    <div class="form-group mb-50">
                        <label class="label" for="user_comment">Комментарий к заказу</label>
                        <input class="input input-20" type="text" id="user_comment">
                    </div>

                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-100 btn-regular btn-orange circleflash">
                            <span class="btn__text">
                                <a href="/checkout-step-two-page">Дальше</a>
                            </span>
                            <span class="circle"></span>
                        </button>
                    </div>
                </form>
            </div>
        </section>
    </main>
    <!-- /.main -->
<?php
get_footer();