<?php
/**
 * Template name: Home page
 */

get_header();
?>
    <main class="main">
        <section class="banner" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/banner.png);">
            <div class="banner__inner">
                <div class="banner__text">
                    Цены товаров, указанные на сайте, актуальны при заказе от <span>30 000</span> рублей. 
                    Точная цена каждого товара отображается в карточке товара, либо 
                    при наведении курсора на информационный значок.
                </div>
                <div class="banner__advantages banner-advantages">
                    <div class="banner-advantages__item">
                        <div class="banner-advantages__icon">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icon/advantages/guarantee.png" alt="">
                        </div>
                        <div class="banner-advantages__text"> 
                            Гарантия <br>
                            качества
                        </div>
                    </div>
    
                    <div class="banner-advantages__item">
                        <div class="banner-advantages__icon">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icon/advantages/cheap.png" alt="">
                        </div>
                        <div class="banner-advantages__text"> 
                            Доступные <br>
                            цены
                        </div>
                    </div>
    
                    <div class="banner-advantages__item">
                        <div class="banner-advantages__icon">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icon/advantages/delivery.png" alt="">
                        </div>
                        <div class="banner-advantages__text"> 
                            Быстрая <br>
                            доставка
                        </div>
                    </div>
    
                    <div class="banner-advantages__item">
                        <div class="banner-advantages__icon">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icon/advantages/client.png" alt="">
                        </div>
                        <div class="banner-advantages__text"> 
                            Довольные <br>
                            клиенты
                        </div>
                    </div>
    
                    <div class="banner-advantages__item">
                        <img class="calyan" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icon/advantages/calyan.png" alt="">
                    </div>
                </div>
            </div>
        </section>
        <!-- /.banner -->
    
        <section class="popular">
            <div class="section-title">популярные  продажи</div>
            <div class="popular-products slider-wrap">
               
                <div class="swiper-container product-slider">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide product-slider__item">
                            <div class="product-slider__image">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/products/1.png" alt="">
                            </div>
                            <a href="/single-page/" class="product-slider__name">
                                Pharaon Premium M1007
                            </a>
                            <div class="product-slider__price">
                                3780 руб
                                <span class="product-price-info">
                                    <ul class="product-price__list">
                                        <li>
                                            <span>3500 руб</span>
                                            При покупке товара на сумму от 30.000 рублей
                                        </li>
                                        <li>
                                            <span>3380 руб</span>
                                            При покупке товара на сумму от 100.000 рублей
                                        </li>
                                        <li>
                                            <span>2980 руб</span>
                                            При покупке товара на сумму от 200.000 рублей
                                        </li>
                                    </ul>
                                </span>
                            </div>
                            <div class="product-slider__bottom">
                                <div class="product-slider__counter product-counter">
                                    <button type="button" class="product-counter__add">-</button>
                                    <input class="product-counter__number" value="1" type="text">
                                    <button type="button" class="product-counter__remove">+</button>
                                </div>
                                <button type="submit" class="btn btn-regular btn-orange circleflash">
                                    <span class="btn__text">В корзину</span>
                                    <span class="circle"></span>
                                </button>
                            </div>
                        </div>
    
                        <div class="swiper-slide product-slider__item">
                            <div class="product-slider__image">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/products/2.png" alt="">
                                <div class="product-badge product-best-price">
                                    Лучшая <br> цена на <br> рынке
                                </div>
                            </div>
                            <a href="/single-page/" class="product-slider__name">
                                M18 Virginia Gold 50 г 
                            </a>
                            <div class="product-slider__price">
                                250 руб
                                <span class="product-price-info">
                                    <ul class="product-price__list">
                                        <li>
                                            <span>3500 руб</span>
                                            При покупке товара на сумму от 30.000 рублей
                                        </li>
                                        <li>
                                            <span>3380 руб</span>
                                            При покупке товара на сумму от 100.000 рублей
                                        </li>
                                        <li>
                                            <span>2980 руб</span>
                                            При покупке товара на сумму от 200.000 рублей
                                        </li>
                                    </ul>
                                </span>
                            </div>
                            <div class="product-slider__bottom">
                                <div class="product-slider__counter product-counter">
                                    <button type="button" class="product-counter__add">-</button>
                                    <input class="product-counter__number" value="1" type="text">
                                    <button type="button" class="product-counter__remove">+</button>
                                </div>
                                <button type="submit" class="btn btn-regular btn-orange circleflash">
                                    <span class="btn__text">В корзину</span>
                                    <span class="circle"></span>
                                </button>
                            </div>
                        </div>
    
                        <div class="swiper-slide product-slider__item">
                            <div class="product-slider__image">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/products/3.png" alt="">
                            </div>
                            <a href="/single-page/" class="product-slider__name">
                                Уголь для кальяна CocoLoco 72 шт 
                            </a>
                            <div class="product-slider__price">
                                1560 руб
                                <span class="product-price-info">
                                    <ul class="product-price__list">
                                        <li>
                                            <span>3500 руб</span>
                                            При покупке товара на сумму от 30.000 рублей
                                        </li>
                                        <li>
                                            <span>3380 руб</span>
                                            При покупке товара на сумму от 100.000 рублей
                                        </li>
                                        <li>
                                            <span>2980 руб</span>
                                            При покупке товара на сумму от 200.000 рублей
                                        </li>
                                    </ul>
                                </span>
                            </div>
                            <div class="product-slider__bottom">
                                <div class="product-slider__counter product-counter">
                                    <button type="button" class="product-counter__add">-</button>
                                    <input class="product-counter__number" value="1" type="text">
                                    <button type="button" class="product-counter__remove">+</button>
                                </div>
                                <button type="submit" class="btn btn-regular btn-orange circleflash">
                                    <span class="btn__text">В корзину</span>
                                    <span class="circle"></span>
                                </button>
                            </div>
                        </div>
    
                        <div class="swiper-slide product-slider__item">
                            <div class="product-slider__image">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/products/4.png" alt="">
                            </div>
                            <a href="/single-page/" class="product-slider__name">
                                Чаша XK Turkish (Белая глина) 
                            </a>
                            <div class="product-slider__price">
                                320 руб
                                <span class="product-price-info">
                                    <ul class="product-price__list">
                                        <li>
                                            <span>3500 руб</span>
                                            При покупке товара на сумму от 30.000 рублей
                                        </li>
                                        <li>
                                            <span>3380 руб</span>
                                            При покупке товара на сумму от 100.000 рублей
                                        </li>
                                        <li>
                                            <span>2980 руб</span>
                                            При покупке товара на сумму от 200.000 рублей
                                        </li>
                                    </ul>
                                </span>
                            </div>
                            <div class="product-slider__bottom">
                                <div class="product-slider__counter product-counter">
                                    <button type="button" class="product-counter__add">-</button>
                                    <input class="product-counter__number" value="1" type="text">
                                    <button type="button" class="product-counter__remove">+</button>
                                </div>
                                <button type="submit" class="btn btn-regular btn-orange circleflash">
                                    <span class="btn__text">В корзину</span>
                                    <span class="circle"></span>
                                </button>
                            </div>
                        </div>
    
                        <div class="swiper-slide product-slider__item">
                            <div class="product-slider__image">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/products/2.png" alt="">
                            </div>
                            <a href="/single-page/" class="product-slider__name">
                                M18 Virginia Gold 50 г 
                            </a>
                            <div class="product-slider__price">
                                250 руб
                                <span class="product-price-info">
                                    <ul class="product-price__list">
                                        <li>
                                            <span>3500 руб</span>
                                            При покупке товара на сумму от 30.000 рублей
                                        </li>
                                        <li>
                                            <span>3380 руб</span>
                                            При покупке товара на сумму от 100.000 рублей
                                        </li>
                                        <li>
                                            <span>2980 руб</span>
                                            При покупке товара на сумму от 200.000 рублей
                                        </li>
                                    </ul>
                                </span>
                            </div>
                            <div class="product-slider__bottom">
                                <div class="product-slider__counter product-counter">
                                    <button type="button" class="product-counter__add">-</button>
                                    <input class="product-counter__number" value="1" type="text">
                                    <button type="button" class="product-counter__remove">+</button>
                                </div>
                                <button type="submit" class="btn btn-regular btn-orange circleflash">
                                    <span class="btn__text">В корзину</span>
                                    <span class="circle"></span>
                                </button>
                            </div>
                        </div>
    
                        <div class="swiper-slide product-slider__item">
                            <div class="product-slider__image">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/products/3.png" alt="">
                            </div>
                            <a href="/single-page/" class="product-slider__name">
                                Уголь для кальяна CocoLoco 72 шт 
                            </a>
                            <div class="product-slider__price">
                                1560 руб
                                <span class="product-price-info">
                                    <ul class="product-price__list">
                                        <li>
                                            <span>3500 руб</span>
                                            При покупке товара на сумму от 30.000 рублей
                                        </li>
                                        <li>
                                            <span>3380 руб</span>
                                            При покупке товара на сумму от 100.000 рублей
                                        </li>
                                        <li>
                                            <span>2980 руб</span>
                                            При покупке товара на сумму от 200.000 рублей
                                        </li>
                                    </ul>
                                </span>
                            </div>
                            <div class="product-slider__bottom">
                                <div class="product-slider__counter product-counter">
                                    <button type="button" class="product-counter__add">-</button>
                                    <input class="product-counter__number" value="1" type="text">
                                    <button type="button" class="product-counter__remove">+</button>
                                </div>
                                <button type="submit" class="btn btn-regular btn-orange circleflash">
                                    <span class="btn__text">В корзину</span>
                                    <span class="circle"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product-slider__button product-slider-prev"></div>
                <div class="product-slider__button product-slider-next"></div>
            </div>
        </section>
        <!-- /.popular -->
    </main>
    <!-- /.main -->
<?php
get_footer();
