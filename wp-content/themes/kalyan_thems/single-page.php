<?php
/**
 * Template name: Single page
 */

get_header();
?>
    <main class="main">
        <section class="breadcrumb">
            <ul class="breadcrumbs">
                <li class="breadcrumbs__item"><a href="" class="breadcrumbs__link">Главная</a></li>
                <li class="breadcrumbs__item"><a href="" class="breadcrumbs__link">Кальяны</a></li>
                <li class="breadcrumbs__item"><a href="" class="breadcrumbs__link">Pharaon</a></li>
                <li class="breadcrumbs__item"><a href="" class="breadcrumbs__link active">Pharaon Premium M1007</a></li>
            </ul>
        </section>
        <!-- /.breadcrumb -->

        <section class="one-product">
            <div class="one-product__image product-image">
                <div class="one-product__title">Pharaon Premium M1007</div>
                 <div class="product-main-image">
                    <img class="show" src="<?php bloginfo('template_directory') ?>/assets/img/single-page/main.png" alt="" data-product-main="1">
                    <img src="<?php bloginfo('template_directory') ?>/assets/img/single-page/2.png" alt="" data-product-main="2">
                    <img src="<?php bloginfo('template_directory') ?>/assets/img/single-page/3.png" alt="" data-product-main="3">
                    <img src="<?php bloginfo('template_directory') ?>/assets/img/single-page/4.png" alt="" data-product-main="4">
                    <img src="<?php bloginfo('template_directory') ?>/assets/img/single-page/1.png" alt="" data-product-main="5">
                    <img src="<?php bloginfo('template_directory') ?>/assets/img/single-page/2.png" alt="" data-product-main="6">
                    <img src="<?php bloginfo('template_directory') ?>/assets/img/single-page/3.png" alt="" data-product-main="7">
                    <img src="<?php bloginfo('template_directory') ?>/assets/img/single-page/4.png" alt="" data-product-main="8">
                 </div>
                <div class="product-thumb-image">
                    <div class="product-thumb-image__item" data-product-thumb="1">
                        <img src="<?php bloginfo('template_directory') ?>/assets/img/single-page/main.png" alt="">
                    </div>
                    <div class="product-thumb-image__item" data-product-thumb="2">
                        <img src="<?php bloginfo('template_directory') ?>/assets/img/single-page/2.png" alt="">
                    </div>
                    <div class="product-thumb-image__item" data-product-thumb="3">
                        <img src="<?php bloginfo('template_directory') ?>/assets/img/single-page/3.png" alt="">
                    </div>
                    <div class="product-thumb-image__item" data-product-thumb="4">
                        <img src="<?php bloginfo('template_directory') ?>/assets/img/single-page/4.png" alt="">
                    </div>

                    <div class="product-thumb-image__item" data-product-thumb="5">
                        <img src="<?php bloginfo('template_directory') ?>/assets/img/single-page/1.png" alt="">
                    </div>
                    <div class="product-thumb-image__item" data-product-thumb="6">
                        <img src="<?php bloginfo('template_directory') ?>/assets/img/single-page/2.png" alt="">
                    </div>
                    <div class="product-thumb-image__item" data-product-thumb="7">
                        <img src="<?php bloginfo('template_directory') ?>/assets/img/single-page/3.png" alt="">
                    </div>
                    <div class="product-thumb-image__item" data-product-thumb="8">
                        <img src="<?php bloginfo('template_directory') ?>/assets/img/single-page/4.png" alt="">
                    </div>
                </div>
            </div>
            <div class="one-product__left">
                <div class="one-product__attention">
                    <div class="one-product__price">3780 руб</div>
                    <div class="one-product__alert">
                        <strong>Внимание!</strong> Оптовые цены актуальны при оформлении заказа не менее, чем на 30.000 рублей! То есть, чтобы получить 
                        товар по ценам на сайте, при оформлении заказа сумма товаров в корзине не должна составлять менее, чем тридцать
                        тысяч рублей. Ждем ваших заказов, спасибо!
                    </div>
                </div>
                
                <div class="one-product__info">
                    <div class="one-product__sale product-sale">
                        <div class="product-sale__item">
                            <span>3500 руб</span>
                            При покупке товара на сумму от 30.000 рублей
                        </div>

                        <div class="product-sale__item">
                            <span>3380 руб</span>
                            При покупке товара на сумму от 100.000 рублей
                        </div>

                        <div class="product-sale__item">
                            <span>2980 руб</span>
                            При покупке товара на сумму от 200.000 рублей
                        </div>

                        <div class="product-slider__bottom one-product__buttons">
                            <div class="product-slider__counter product-counter">
                                <button type="button" class="product-counter__add">-</button>
                                <input class="product-counter__number" value="1" type="text">
                                <button type="button" class="product-counter__remove">+</button>
                            </div>
                            <button type="submit" class="btn btn-regular btn-orange circleflash">
                                <span class="btn__text">В корзину</span>
                                <span class="circle"></span>
                            </button>
                        </div>
                        
                    </div>

                    <div class="product-performance">
                        <div class="product-performance__item">Материал              <i class="product-performance__dots"></i><span class="product-performance__span">Нержавеющая сталь</span></div>
                        <div class="product-performance__item">Тип соединения        <i class="product-performance__dots"></i><span class="product-performance__span">Черный </span></div>
                        <div class="product-performance__item">Цвет                  <i class="product-performance__dots"></i><span class="product-performance__span">Китай</span></div>
                        <div class="product-performance__item">Страна производитель  <i class="product-performance__dots"></i><span class="product-performance__span">Есть</span></div>
                        <div class="product-performance__item">Диффузор              <i class="product-performance__dots"></i><span class="product-performance__span">На резьбе</span></div>
                        <div class="product-performance__more"><span>Еще характеристики</span><i class="i-down"></i></div>
                    </div>
                </div>

                <div class="product-attribute">
                    <div class="product-attribute__item">
                        Акция - <span>Нет</span>
                    </div>
                    <div class="product-attribute__item">
                        В наличии - <span>Да</span>
                    </div>
                </div>

                <div class="product-description">
                    <p>
                        Кальян Pharaon Premium x1011 all black высотой 71см предназначен для использования в домашних условиях. Черно-синяя расцветка шахты и восточный 
                        ручной работы узор выполнены специальной краской, которая не трескается, не смывается и не тускнеет. Для производства кальяна были использованы 
                        качественные материалы. Шахта с колбой соединяется составным способом. Шахта сделана из металла, которому не страшна коррозия, колба выполнена 
                        из толстостенного стекла. Произведен в ОАЭ.
                    </p>

                    <p>
                        Honey Pharaon Premium – это компактный (высота шахты всего 40 см), разборный кальян российского производства из бюджетного сегмента с 
                        великолепными характеристиками. Внутренний диаметр шахты 13 мм обеспечивает легкую и прекрасную тягу, диффузор не предусмотрен, можно 
                        использовать универсальные модели из пластика или силикона. Продувка состоит из 5 каналов – весь обьем колбы выдувается с первой попытки, дым идет 
                        вертикально вверх, под блюдце. Коннектор шланга соединяется с основанием из полиацеталя с помощью о-рингов. Все части, помимо основания 
                        сделаны из нержавеющей стали марки AISI 304. В комплекте также имеется силиконовый soft touch шланг и мундштук, выполненный из нержавейки в 
                        едином стиле с рисунком шахты. Для полноценного покура остается докупить лишь колбу и чашу.
                    </p>

                    <p>
                        Классический кальян неизменно остается наиболее популярным вариантом, несмотря на то, что выпускаются разные необычные формы. Однако нет 
                        ничего лучше проверенного временем кальяна в традиционном стиле. А если немного изменить его дизайн на более современный, то можно получить 
                        отличную модель, которая будет хорошо смотреться в интерьере и одновременно обладать всеми традиционными плюсами. Можно убедиться в этом самим, 
                        если купить кальян Pharaon 2006 (Фараон 2006).
                    </p>

                    <p>
                        По форме - это традиционный восточный кальян, такие силуэты свойственны арабским или сирийским моделям. Дизайн этого изделия - современный, в 
                        стиле минимализма, без всяческих излишеств и вычурного декора. Цена кальяна Pharaon 2006 (Фараон 2006) вполне доступна для большинства 
                        кальянщиков, приобрести такое изделие можно без ущерба для бюджета, кроме того, в комплект с ним уже входят все необходимые приспособления, даже 
                        щипцы для угля. Для производства кальяна использовались качественные и безопасные материалы, они не впитывают запахов, а также не выделяют 
                        вредных веществ при нагревании. У нас можно купить кальян Pharaon 2006 (Фараон 2006) с доставкой, воспользовавшись всеми удобными вам условиями.
                    </p>
                </div>

            </div>
        </section>
        <!-- /.product -->
    </main>
    <!-- /.main -->
<?php
get_footer();