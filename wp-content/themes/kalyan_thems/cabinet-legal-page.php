<?php
/**
 * Template name: Cabinet Legal page
 */

get_header();
?>
    <main class="main">
        <section class="s-cabinet">
            <div class="section-bold-title">Личный кабинет - анкета юр.лица</div>
            <div class="cabinet-edit cabinet-edit__grid">
                <form action="">
                    <div class="cabinet-grid">
                        <div class="cabinet-grid__item">
                            <div class="form-group mb-20">
                                <label class="label" for="userFIO">ФИО</label>
                                <input class="input input-20" type="text" id="userFIO" value="Иванов Иван Иванович">
                                <label for="userFIO" class="edit-label"><i class="i-edit"></i></label>
                            </div>

                            <div class="form-group mb-20">
                                <label class="label" for="user_address">Адрес</label>
                                <input class="input input-20" type="text" id="user_address" value="г. Москва, Новый Арбат 178, 3 корпус, кв 44">
                                <label for="user_address" class="edit-label"><i class="i-edit"></i></label>
                            </div>

                            <div class="form-group mb-20">
                                <label class="label" for="user_phone">Номер телефона</label>
                                <input class="input input-20" type="text" id="user_phone" value="+7 999 879-87-32">
                                <label for="user_phone" class="edit-label"><i class="i-edit"></i></label>
                            </div>

                            <div class="form-group mb-20">
                                <label class="label" for="user_email">E-mail</label>
                                <input class="input input-20" type="text" id="user_email" value="ivanov-i-i@yandex.ru">
                                <label for="user_email" class="edit-label"><i class="i-edit"></i></label>
                            </div>

                            <div class="form-group mb-20">
                                <label class="label" for="user_password">Пароль</label>
                                <input class="input input-20" type="text" id="user_password" value="*********************">
                                <label for="user_password" class="edit-label"><i class="i-edit"></i></label>
                            </div>
                        </div>

                        <div class="cabinet-grid__item">
                            <div class="form-group mb-20">
                                <label class="label" for="userLegalName">Название юр.дица</label>
                                <input class="input input-20" type="text" id="userLegalName" value="ИП Иванов">
                                <label for="userLegalName" class="edit-label"><i class="i-edit"></i></label>
                            </div>

                            <div class="form-group mb-20">
                                <label class="label" for="userTIN">ИНН</label>
                                <input class="input input-20" type="text" id="userTIN" value="98787221357891231453621454">
                                <label for="userTIN" class="edit-label"><i class="i-edit"></i></label>
                            </div>

                            <div class="form-group mb-20">
                                <label class="label" for="user_phone">КПП</label>
                                <input class="input input-20" type="text" id="userKPP" value="3215478656544">
                                <label for="userKPP" class="edit-label"><i class="i-edit"></i></label>
                            </div>

                            <div class="form-group mb-20">
                                <label class="label" for="GeneralDirector">Ген.директор</label>
                                <input class="input input-20" type="text" id="GeneralDirector" value="Иванов Иван Иванович">
                                <label for="GeneralDirector" class="edit-label"><i class="i-edit"></i></label>
                            </div>

                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-100 btn-regular btn-orange mr-10 circleflash">
                                    <span class="btn__text">Выйти</span>
                                    <span class="circle"></span>
                                </button>

                                <button type="submit" class="btn btn-100 btn-regular btn-orange circleflash">
                                    <span class="btn__text">Сохранить</span>
                                    <span class="circle"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </main>
    <!-- /.main -->
<?php
get_footer();