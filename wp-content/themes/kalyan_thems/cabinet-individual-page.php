<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 * 
 * Template name: Cabinet Individual page
 */

get_header();
?>
    <main class="main">
        <section class="s-cabinet">
            <div class="section-bold-title">Личный кабинет - анкета физ.лица</div>
            <div class="cabinet-edit">
                <form action="">
                    <div class="form-group mb-20">
                        <label class="label" for="user_fullname">ФИО</label>
                        <input class="input input-20" type="text" id="user_fullname" value="Иванов Иван Иванович">
                        <label for="user_fullname" class="edit-label"><i class="i-edit"></i></label>
                    </div>

                    <div class="form-group mb-20">
                        <label class="label" for="user_address">Адрес</label>
                        <input class="input input-20" type="text" id="user_address" value="г. Москва, Новый Арбат 178, 3 корпус, кв 44">
                        <label for="user_address" class="edit-label"><i class="i-edit"></i></label>
                    </div>

                    <div class="form-group mb-20">
                        <label class="label" for="user_phone">Номер телефона</label>
                        <input class="input input-20" type="text" id="user_phone" value="+7 999 879-87-32">
                        <label for="user_phone" class="edit-label"><i class="i-edit"></i></label>
                    </div>

                    <div class="form-group mb-20">
                        <label class="label" for="user_email">E-mail</label>
                        <input class="input input-20" type="text" id="user_email" value="ivanov-i-i@yandex.ru">
                        <label for="user_email" class="edit-label"><i class="i-edit"></i></label>
                    </div>

                    <div class="form-group mb-20">
                        <label class="label" for="user_password">Пароль</label>
                        <input class="input input-20" type="text" id="user_password" value="*********************">
                        <label for="user_password" class="edit-label"><i class="i-edit"></i></label>
                    </div>

                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-100 btn-regular btn-orange mr-10 circleflash">
                            <span class="btn__text">Выйти</span>
                            <span class="circle"></span>
                        </button>

                        <button type="submit" class="btn btn-100 btn-regular btn-orange circleflash">
                            <span class="btn__text">Сохранить</span>
                            <span class="circle"></span>
                        </button>
                    </div>
                </form>
            </div>
        </section>
    </main>
    <!-- /.main -->
<?php
get_footer();
