
    <footer class="footer">
        <div class="footer__text">
            <span>intoco</span> - Все для кальянов по оптовым ценам
        </div>
        <div class="footer__social">
            <a href=""><i class="i-telegram"></i></a>
            <a href=""><i class="i-instagram"></i></a>
            <a href=""><i class="i-vk"></i></a>
            <a href=""><i class="i-facebook"></i></a>
            <a href=""><i class="i-whatsapp"></i></a>
        </div>
        <div class="footer__btn">
            <button type="submit" class="circleflash modal-btn" data-modal-button="requestPrice">
                <span class="btn__text">Запросить прайс</span>
                <span class="circle"></span>
            </button>
        </div>
    </footer>
    <!-- /.footer -->

    <!-- Modal popup -->
    <div class="modal" data-modal-content="feedback">
        <div class="modal__block">
            <button type="button" class="modal__close"></button>
            <h2 class="modal__title">Обратная связь</h2>
            <p class="modal__text">
                Оставьте свое имя и номер телефона, <br>
                мы перезвоним Вам в самое ближайшее время
            </p>
            <form action="">
                <div class="form-group mb-15">
                    <input class="input" type="text" placeholder="Имя" name="userName" required>
                </div>

                <div class="form-group mb-15">
                    <input class="input" type="text" placeholder="Номер телефона" name="userPhone" required>
                </div>

                <div class="form-group mb-20">
                    <textarea class="textarea" name="userComment" id="userComment" cols="30" rows="3" placeholder="Ваш комменатрий"></textarea>
                </div>

                <div class="form-group text-left">
                    <button type="submit" class="btn btn-big btn-orange circleflash">
                        <span class="btn__text">Отправить</span>
                        <span class="circle"></span>
                    </button>
                </div>
            </form>
        </div>
    </div>

    <div class="modal modal__login" data-modal-content="login">
        <div class="modal__block">
            <button type="button" class="modal__close"></button>
            <h2 class="modal__title mb-40">Войти в личный кабинет</h2>
            <form action="">
                <div class="form-group mb-15">
                    <input class="input" type="email" placeholder="E-mail" name="userEmail" required>
                </div>

                <div class="form-group mb-15">
                    <input class="input" type="password" placeholder="Пароль" name="userPassword" required>
                </div>

                <div class="form-group mb-15">
                    <input type="checkbox" id="remember" hidden>
                    <label for="remember" class="checkbox">Запомнить меня</label>
                </div>

                <div class="form-group text-left mb-15">
                    <button type="submit" class="btn btn-big btn-orange circleflash login-btn">
                        <span class="btn__text">Войти</span>
                        <span class="circle"></span>
                    </button>
                </div>

                <div class="form-group text-left mb-15">
                    <a href="">Забыли пароль?</a>
                </div>

                <div class="form-group text-left">
                    <a href="register.html" target="_blank">Зарегистрироваться?</a>
                </div>
            </form>
        </div>
    </div>

    <div class="modal" data-modal-content="requestPrice">
        <div class="modal__block">
            <button type="button" class="modal__close"></button>
            <h2 class="modal__title">Запрос на прайс-лист</h2>
            <p class="modal__text">
                Заполните форму ниже и мы <br>
                вышлем Вам прайс-лист на указанную почту
            </p>
            <form action="">
                <div class="form-group mb-15">
                    <input class="input" type="text" placeholder="Имя" name="userName" required>
                </div>

                <div class="form-group mb-15">
                    <input class="input" type="email" placeholder="E-mail" name="userEmail" required>
                </div>

                <div class="form-group mb-15">
                    <input class="input" type="text" placeholder="Номер телефона" name="userPhone" required>
                </div>

                <div class="form-group text-left">
                    <button type="submit" class="btn btn-big btn-orange circleflash">
                        <span class="btn__text">Отправить</span>
                        <span class="circle"></span>
                    </button>
                </div>
            </form>
        </div>
    </div>
    


    <!-- All Scripts  -->
    <script src="<?php bloginfo('template_directory') ?>/assets/lib/jquery-3.5.1.min.js"></script>
    <script src="<?php bloginfo('template_directory') ?>/assets/lib/swiper-bundle.min.js"></script>
    <script src="<?php bloginfo('template_directory') ?>/assets/lib/jquery.nice-select.min.js"></script>
    <script src="<?php bloginfo('template_directory') ?>/assets/js/custom.js"></script>
</body>
</html>