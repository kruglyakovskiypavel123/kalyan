<!-- All Scripts  -->
<script src="<?php bloginfo('template_directory') ?>/assets/lib/jquery-3.5.1.min.js"></script>
    <script src="<?php bloginfo('template_directory') ?>/assets/lib/swiper-bundle.min.js"></script>
    <script src="<?php bloginfo('template_directory') ?>/assets/lib/jquery.nice-select.min.js"></script>
    <script src="<?php bloginfo('template_directory') ?>/assets/js/custom.js"></script>
</body>
</html>