<?php
/**
 * Template name: Search page
 */

get_header();
?>
    <main class="main">
        <section class="s-search">
            <div class="section-bold-title">Поиск</div>
            <div class="search-result-text">По вашему запросу найдено 4 товара</div>
            <div class="search-result">
                <div class="product-slider__item">
                    <div class="product-slider__image">
                        <img src="<?php bloginfo('template_directory') ?>/assets/img/search/1.png" alt="">
                    </div>
                    <a href="" class="product-slider__name">
                        Pharaon Premium M1007
                    </a>
                    <div class="product-slider__price">
                        3780 руб
                        <span class="product-price-info">
                            <ul class="product-price__list">
                                <li>
                                    <span>3500 руб</span>
                                    При покупке товара на сумму от 30.000 рублей
                                </li>
                                <li>
                                    <span>3380 руб</span>
                                    При покупке товара на сумму от 100.000 рублей
                                </li>
                                <li>
                                    <span>2980 руб</span>
                                    При покупке товара на сумму от 200.000 рублей
                                </li>
                            </ul>
                        </span>
                    </div>
                    <div class="product-slider__bottom">
                        <div class="product-slider__counter product-counter">
                            <button type="button" class="product-counter__add">-</button>
                            <input class="product-counter__number" value="1" type="text">
                            <button type="button" class="product-counter__remove">+</button>
                        </div>
                        <button type="submit" class="btn btn-regular btn-orange circleflash">
                            <span class="btn__text">В корзину</span>
                            <span class="circle"></span>
                        </button>
                    </div>
                </div>

                <div class="product-slider__item">
                    <div class="product-slider__image">
                        <img src="<?php bloginfo('template_directory') ?>/assets/img/search/2.png" alt="">
                        <div class="product-badge product-best-price">
                            Лучшая <br> цена на <br> рынке
                        </div>
                    </div>
                    <a href="" class="product-slider__name">
                        SkySeven Vector Gray Light Green
                    </a>
                    <div class="product-slider__price">
                        3780 руб
                        <span class="product-price-info">
                            <ul class="product-price__list">
                                <li>
                                    <span>3500 руб</span>
                                    При покупке товара на сумму от 30.000 рублей
                                </li>
                                <li>
                                    <span>3380 руб</span>
                                    При покупке товара на сумму от 100.000 рублей
                                </li>
                                <li>
                                    <span>2980 руб</span>
                                    При покупке товара на сумму от 200.000 рублей
                                </li>
                            </ul>
                        </span>
                    </div>
                    <div class="product-slider__bottom">
                        <div class="product-slider__counter product-counter">
                            <button type="button" class="product-counter__add">-</button>
                            <input class="product-counter__number" value="1" type="text">
                            <button type="button" class="product-counter__remove">+</button>
                        </div>
                        <button type="submit" class="btn btn-regular btn-orange circleflash">
                            <span class="btn__text">В корзину</span>
                            <span class="circle"></span>
                        </button>
                    </div>
                </div>

                <div class="product-slider__item">
                    <div class="product-slider__image">
                        <img src="<?php bloginfo('template_directory') ?>/assets/img/search/3.png" alt="">
                    </div>
                    <a href="" class="product-slider__name">
                        Amy Deluxe SS 09 Stick Steel
                    </a>
                    <div class="product-slider__price">
                        3780 руб
                        <span class="product-price-info">
                            <ul class="product-price__list">
                                <li>
                                    <span>3500 руб</span>
                                    При покупке товара на сумму от 30.000 рублей
                                </li>
                                <li>
                                    <span>3380 руб</span>
                                    При покупке товара на сумму от 100.000 рублей
                                </li>
                                <li>
                                    <span>2980 руб</span>
                                    При покупке товара на сумму от 200.000 рублей
                                </li>
                            </ul>
                        </span>
                    </div>
                    <div class="product-slider__bottom">
                        <div class="product-slider__counter product-counter">
                            <button type="button" class="product-counter__add">-</button>
                            <input class="product-counter__number" value="1" type="text">
                            <button type="button" class="product-counter__remove">+</button>
                        </div>
                        <button type="submit" class="btn btn-regular btn-orange circleflash">
                            <span class="btn__text">В корзину</span>
                            <span class="circle"></span>
                        </button>
                    </div>
                </div>

                <div class="product-slider__item">
                    <div class="product-slider__image">
                        <img src="<?php bloginfo('template_directory') ?>/assets/img/search/4.png" alt="">
                        <div class="product-badge product-bestseller">
                            Хит <br>
                            продаж
                        </div>
                    </div>
                    <a href="" class="product-slider__name">
                        Amy 4-Star 410 Silver Red
                    </a>
                    <div class="product-slider__price">
                        3780 руб
                        <span class="product-price-info">
                            <ul class="product-price__list">
                                <li>
                                    <span>3500 руб</span>
                                    При покупке товара на сумму от 30.000 рублей
                                </li>
                                <li>
                                    <span>3380 руб</span>
                                    При покупке товара на сумму от 100.000 рублей
                                </li>
                                <li>
                                    <span>2980 руб</span>
                                    При покупке товара на сумму от 200.000 рублей
                                </li>
                            </ul>
                        </span>
                    </div>
                    <div class="product-slider__bottom">
                        <div class="product-slider__counter product-counter">
                            <button type="button" class="product-counter__add">-</button>
                            <input class="product-counter__number" value="1" type="text">
                            <button type="button" class="product-counter__remove">+</button>
                        </div>
                        <button type="submit" class="btn btn-regular btn-orange circleflash">
                            <span class="btn__text">В корзину</span>
                            <span class="circle"></span>
                        </button>
                    </div>
                </div>
            </div>
        </section>
    </main>
    <!-- /.main -->
<?php
get_footer();