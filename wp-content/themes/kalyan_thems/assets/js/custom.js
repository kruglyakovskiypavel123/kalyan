$(document).ready(function(){
    $('.search-btn').on('click', function() {
        $(this).parent('.search').toggleClass('show');
    });

    $('.modal-btn').on('click', function () {
        $('.modal-btn').removeClass('active');

        $(this).addClass('active');

        $('.modal').removeClass("show");

        var data = $(this).data('modal-button');

        $('.modal[data-modal-content=' + data + ']').addClass("show");
    });

    $('.modal__close').on('click', function(){
        $(this).parents('.modal').removeClass('show');
    });

    $('.menu-btn').on('click', function(){
        $(this).toggleClass('active');
        $('.right-menu').toggleClass('active');
    });

    var swiper = new Swiper('.product-slider', {
        loop: true,
        navigation: {
          nextEl: '.product-slider-next',
          prevEl: '.product-slider-prev',
        },
        breakpoints: {
            // when window width is <= 576px
            576: {
                slidesPerView: 1,
                spaceBetween: 10,
            },
            // when window width is <= 768px
            768: {
                slidesPerView: 2,
                spaceBetween: 15,
            },
            // when window width is <= 1366px
            1366: {
                slidesPerView: 3,
                spaceBetween: 20,
            },
            // when window width is <= 1440px
            1440: {
                slidesPerView: 3,
                spaceBetween: 20,
            },
            // when window width is <= 1600px
            1600: {
                slidesPerView: 3,
                spaceBetween: 30,
            },
            // when window width is <= 1920px
            1920: {
                slidesPerView: 4,
                spaceBetween: 60,
            }
        }
    });

    $(window).scroll(function() {
        if ($(this).scrollTop()) {
            $('.scroll-top').addClass('show');
        } else {
            $('.scroll-top').removeClass('show');
        }
    });
    
    $(".scroll-top").click(function() {
        $("html, body").animate({scrollTop: 0}, 800);
    });

    $('.product-thumb-image__item').on('click', function () {
        $('.product-thumb-image__item').removeClass('active');

        $(this).addClass('active');

        $('.product-main-image img').removeClass("show");

        var data = $(this).data('product-thumb');

        $('.product-main-image img[data-product-main=' + data + ']').addClass("show");
    });

    $('#individual:radio').change(function(){
        if($(this).is(":checked")) {
            $('.form-individual').removeClass("disable");
            $('.form-juridical').addClass("disable");
        } else {
            $('.form-individual').addClass("disable");
            $('.form-juridical').removeClass("disable");
        }
    });

    $('#juridical:radio').change(function(){
        if($(this).is(":checked")) {
            $('.form-individual').addClass("disable");
            $('.form-juridical').removeClass("disable");
        } else {
            $('.form-individual').removeClass("disable");
            $('.form-juridical').addClass("disable");
        }
    });

    $('.select').niceSelect();

    $('.catalog-sub-list').hover( function () {
        $(this).find('ul').slideToggle('fast');
    })


    let nonLinearSlider = document.getElementById('nonlinear');

    noUiSlider.create(nonLinearSlider, {
        connect: true,
        behaviour: 'tap',
        start: [1500, 9500],
        range: {
            'min': [500],
            'max': [99000]
        },
        format: wNumb({
            decimals: 1,
            thousand: ' ',
            suffix: ''
        })
    });

    let nodes = [
        document.getElementById('lower-value'), // 0
        document.getElementById('upper-value')  // 1
    ];

    nonLinearSlider.noUiSlider.on('update', function (values, handle, unencoded, isTap, positions) {
        nodes[handle].innerHTML = values[handle] + '' + positions[handle].toFixed() + '';
    });

    $('.btn-dropdown').on('click', function () {
        $(this).toggleClass('active');
        $('.s-sub-filter').slideToggle('fast');
    });

    $('#catalog-btn').on('click', function () {
        $('.s-catalog').addClass('active');
    })

    $('#filter-btn').on('click', function () {
        $('.s-filter').addClass('active');
    })

    $('.btn-close').on('click', function () {
        $('.s-catalog').removeClass('active');
        $('.s-filter').removeClass('active');
    });
});
