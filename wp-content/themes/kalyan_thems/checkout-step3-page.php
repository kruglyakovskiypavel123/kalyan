<?php
/**
 * Template name: Checkout Step three page
 */

get_header();
?>
    <main class="main">
        <section class="s-checkout">
            <div class="section-bold-title">Оформление заказа</div>
            <div class="checkout-steps">
                <div class="checkout-steps__title disable">
                    <span class="color-orange">Шаг 1.</span> Контактные данные
                </div>

                <div class="checkout-steps__title disable">
                    <span class="color-orange">Шаг 2.</span> Способ доставки
                </div>

                <div class="checkout-steps__title">
                    <span class="color-orange">Шаг 3.</span> Подтверждение и оплата
                </div>
            </div>
            <div class="checkout-block">
                <form action="">
                    <div class="checkout-block__title">Проверьте еще раз достоверность заказа</div>
                    <div class="checkout-block__title">Сумма 46460 руб</div>

                    <div class="receiver-label">ФИО получателя</div>
                    <div class="receiver-item">Иванов Иван Иванович</div>

                    <div class="receiver-label">Полный адрес доставки, Индекс, все через запятую</div>
                    <div class="receiver-item">Россия, г. Москва, Новый Арбат 178, 3 корпус, кв 44, индекс 768493</div>

                    <div class="receiver-label">Номер телефона получателя</div>
                    <div class="receiver-item">+7 999 879-87-32</div>

                    <div class="receiver-label">E-mail получателя</div>
                    <div class="receiver-item mb-25">ivanov-i-i@yandex.ru</div>

                    <div class="payment-method">
                        <div class="payment-method__title">
                            Способ оплаты
                        </div>
                        <div class="payment-method__types">
                            <div class="mb-15 payment-method__top">
                                <div class="form-group">
                                    <input type="checkbox" id="payment-online" hidden checked>
                                    <label for="payment-online" class="checkbox payment__checkbox">Оплата онлайн или картой</label>
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" id="payment-cash" hidden>
                                    <label for="payment-cash" class="checkbox payment__checkbox">Оплата наличными курьеру</label>
                                </div>
                            </div>
                            <div class="form-group mb-15">
                                <input type="checkbox" id="payment-legal" hidden>
                                <label for="payment-legal" class="checkbox payment__checkbox">Выставление счета для оплаты юр.лицам</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group payment-submit">
                        <button class="btn btn-100 btn-regular btn-orange circleflash">
                            <span class="btn__text">Подтвердить</span>
                            <span class="circle"></span>
                        </button>
                    </div>
                </form>
            </div>
        </section>
    </main>
    <!-- /.main -->
<?php
get_footer();