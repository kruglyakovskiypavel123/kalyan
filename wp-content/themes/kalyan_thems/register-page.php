<?php
/**
 * Template name: Register page
 */

get_header();
?>
    <main class="main">
        <div class="register-title">Регистрация</div>
        <div class="register-form">
            <div class="form-individual">
                <div class="form-group mb-30">
                    <input type="radio" id="individual" hidden name="register" checked>
                    <label for="individual" class="checkbox checkbox__register">Физическое лицо</label>
                </div>
                <form action="">
                    <div class="form-grid-2">
                        <div class="form-group">
                            <input class="input input__dark" type="text" placeholder="ФИО*" name="userFIO" required>
                        </div>

                        <div class="form-group">
                            <input class="input input__dark" type="text" placeholder="Город*" name="userCity" required>
                        </div>

                        <div class="form-group">
                            <input class="input input__dark" type="text" placeholder="Ваш телефон*" name="userPhone" required>
                        </div>

                        <div class="form-group">
                            <input class="input input__dark" type="password" placeholder="Пароль*" name="userPassword" required>
                        </div>

                        <div class="form-group">
                            <input class="input input__dark" type="email" placeholder="Ваш e-mail*" name="userEmail" required>
                        </div>

                        <div class="form-group">
                            <input class="input input__dark" type="password" placeholder="Подтверждение пароля*" name="userConfPassword" required>
                        </div>

                        <div class="form-group">
                            <select name="" class="select select__dark">
                                <option data-display="Вид деятельности*">Вид деятельности</option>
                                <option value="Кальянная">Кальянная</option>
                                <option value="Кальянная-ресторан">Кальянная-ресторан</option>
                                <option value="Интернет магазин">Интернет магазин</option>
                                <option value="Розничная сеть">Розничная сеть</option>
                                <option value="Оптовая компания">Оптовая компания</option>
                            </select>
                        </div>
                        <div class="form-group d-flex align-items-center justify-content-end">
                            <button type="submit" class="btn btn-regular btn-orange circleflash">
                                <span class="btn__text">Зарегистрироваться</span>
                                <span class="circle"></span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="form-juridical disable">
                <div class="form-group mb-30">
                    <input type="radio" id="juridical" hidden name="register">
                    <label for="juridical" class="checkbox checkbox__register">Юридическое лицо</label>
                </div>
                <form action="">
                    <div class="form-grid-2">
                        <div class="form-group">
                            <input class="input input__dark" type="text" placeholder="ФИО*" name="userFIO" required>
                        </div>

                        <div class="form-group">
                            <input class="input input__dark" type="text" placeholder="КПП*" name="userKPP" required>
                        </div>

                        <div class="form-group">
                            <input class="input input__dark" type="text" placeholder="Ваш телефон*" name="userPhone" required>
                        </div>

                        <div class="form-group">
                            <input class="input input__dark" type="text" placeholder="Ген. директор*" name="userDirector" required>
                        </div>

                        <div class="form-group">
                            <input class="input input__dark" type="email" placeholder="Ваш e-mail*" name="userEmail" required>
                        </div>

                        <div class="form-group">
                            <input class="input input__dark" type="text" placeholder="Город*" name="userCity" required>
                        </div>

                        <div class="form-group">
                            <input class="input input__dark" type="text" placeholder="Название юрлица*" name="userLegalName" required>
                        </div>

                        <div class="form-group">
                            <input class="input input__dark" type="password" placeholder="Пароль*" name="userPassword" required>
                        </div>

                        <div class="form-group">
                            <input class="input input__dark" type="text" placeholder="ИНН*" name="userTIN" required>
                        </div>

                        <div class="form-group">
                            <input class="input input__dark" type="password" placeholder="Подтверждение пароля*" name="userConfPassword" required>
                        </div>

                        <div class="form-group">
                            <select name="" id="" class="select select__dark">
                                <option value="Вид деятельности">Вид деятельности*</option>
                                <option value="Кальянная">Кальянная</option>
                                <option value="Кальянная-ресторан">Кальянная-ресторан</option>
                                <option value="Интернет магазин">Интернет магазин</option>
                                <option value="Розничная сеть">Розничная сеть</option>
                                <option value="Оптовая компания">Оптовая компания</option>
                            </select>
                        </div>
                        <div class="form-group d-flex align-items-center justify-content-end">
                            <button type="submit" class="btn btn-regular btn-orange circleflash">
                                <span class="btn__text">Зарегистрироваться</span>
                                <span class="circle"></span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="register-title">Уважаемые покупатели</div>
        <div class="register-text">
            <p>
                В связи с переходом на новую торговую систему необходимо пройти регистрацию. В регистрационной форме обязательно необходимо указать актуальный адрес электронной 
                почты. На него придут логин и пароль для входа в систему. Если вы уже являетесь нашим клиентом и активно совершали покупки через интернет-магазин, то за вами остаются
                закреплены ваши уровни цен и персональные менеджеры. Обратите внимание! Всем нашим ранее зарегистрированным клиентам на адрес электронной почты в течение 1-2 
                дней будут отправлены данные для входа в личный кабинет. Проверьте Вашу почту ( в т.ч. ящик СПАМ) возможно у вас уже есть логин и пароль!
            </p>
        </div>

        <div class="register-info">
            <p>
                Данный сайт не является рекламой, не предназначен для розничной торговли, <br>
                а служит для предоставления достоверной информации об основных свойствах и характеристиках товара <br>
                для профессиональной аудитории. Мы не продаем продукцию лицам, не достигшим 18 лет. <br>
                (ст. 20 ФЗ №15 «Об охране здоровья граждан»).
            </p>
        </div>

        
    </main>
    <!-- /.main -->
<?php
get_footer();