<!DOCTYPE html>
<html lang="ru">
<head>
    <?php wp_head(); ?>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="<?php bloginfo('template_directory') ?>/assets/css/custom.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory') ?>/assets/css/swiper-bundle.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory') ?>/assets/css/nice-select.css">

</head>
<body>