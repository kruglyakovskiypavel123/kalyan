<?php
/**
 * Template name: Catalog Ugol page
 */

get_header();
?>
    <main class="main">
        <section class="breadcrumb">
            <ul class="breadcrumbs">
                <li class="breadcrumbs__item"><a href="" class="breadcrumbs__link">Главная</a></li>
                <li class="breadcrumbs__item"><a href="" class="breadcrumbs__link active">Уголь</a></li>
            </ul>
        </section>
        <!-- /.breadcrumb -->

        <section class="s-filter">
            <button type="button" class="btn-close"><i class="i-close"></i></button>
            <form action="">
                <select name="" class="select select__filter">
                    <option value="">Сортировка</option>
                    <option value="">Алюминий</option>
                    <option value="">Латунь</option>
                    <option value="">Акрил</option>
                    <option value="">Стекло</option>
                </select>
                <span class="line"></span>

                <div class="range-slider">
                    <label for="nonlinear" class="slider-range__label">По цене</label>

                    <span class="range-slider_value" id="lower-value"></span>

                    <div id="nonlinear"></div>

                    <span class="range-slider_value" id="upper-value"></span>
                </div>

                <span class="line"></span>

                <select name="" class="select select__filter">
                    <option value="">Материал угля</option>
                    <option value="">Алюминий</option>
                    <option value="">Латунь</option>
                    <option value="">Акрил</option>
                    <option value="">Стекло</option>
                </select>
                <span class="line"></span>
                <select name="" class="select select__filter ">
                    <option value="">Вес угля</option>
                    <option value="">Алюминий</option>
                    <option value="">Латунь</option>
                    <option value="">Акрил</option>
                    <option value="">Стекло</option>
                </select>
                <span class="line"></span>
                <select name="" class="select select__filter ">
                    <option value="">Страна производитель</option>
                    <option value="">Алюминий</option>
                    <option value="">Латунь</option>
                    <option value="">Акрил</option>
                    <option value="">Стекло</option>
                </select>
                <span class="line"></span>
                <select name="" class="select select__filter ">
                    <option value="">Размер угля</option>
                    <option value="">Алюминий</option>
                    <option value="">Латунь</option>
                    <option value="">Акрил</option>
                    <option value="">Стекло</option>
                </select>
                <span class="line"></span>
                <div class="form-group stocks-checkbox">
                    <input type="checkbox" id="Stocks" hidden>
                    <label for="Stocks" class="checkbox">Акции</label>
                </div>
                <span class="line"></span>
                <div class="form-group stocks-checkbox">
                    <input type="checkbox" id="Наличие" hidden>
                    <label for="Наличие" class="checkbox">Наличие</label>
                </div>
                <span class="line"></span>
                <div class="form-group stocks-checkbox">
                    <input type="checkbox" id="all" hidden>
                    <label for="all" class="checkbox">Все</label>
                </div>
                <button type="button" class="btn-dropdown"></button>
            </form>
        </section>

        <section class="s-filter s-sub-filter">
            <form action="">
                <select name="" class="select select__filter">
                    <option value="">По-размеру</option>
                    <option value="">Алюминий</option>
                    <option value="">Латунь</option>
                    <option value="">Акрил</option>
                    <option value="">Стекло</option>
                </select>
            </form>
        </section>

        <section class="filter-buttons">
            <button id="catalog-btn" type="submit" class="btn btn-big btn-orange circleflash">
                <span class="btn__text">Каталог</span>
                <span class="circle"></span>
            </button>
            <button id="filter-btn" type="submit" class="btn btn-big btn-orange circleflash">
                <span class="btn__text">Фильтр</span>
                <span class="circle"></span>
            </button>
        </section>

        <section class="s-catalog">
            <button type="button" class="btn-close"><i class="i-close"></i></button>
            <div class="catalog-list">
                <ul>
                    <li class="catalog-sub-list">
                        <a href="">Арт кальян</a>
                        <ul class="">
                            <li><a href="">Adalya 35 gr</a></li>
                            <li><a href="">Adalya 50 gr</a></li>
                        </ul>
                    </li>
                    <li><a href="">Alpha</a></li>
                    <li><a href="">Hookah</a></li>
                    <li><a href="">Amy Avanti</a></li>
                    <li><a href="">Blade Bodo</a></li>
                    <li><a href="">Craft CWP</a></li>
                    <li><a href="">Darkside</a></li>
                    <li><a href="">DSH</a></li>
                    <li><a href="">Euroshisha</a></li>
                    <li><a href="">Fabula</a></li>
                    <li><a href="">Temple 45</a></li>
                    <li><a href="">Honey Sigh</a></li>
                    <li><a href="">Hoob Hookah</a></li>
                    <li><a href="">Box Hookah</a></li>
                    <li><a href="">Tree Karma</a></li>
                    <li><a href="">Khalil Mamoon</a></li>
                    <li><a href="">Maklaud</a></li>
                    <li><a href="">Shisha</a></li>
                    <li><a href="">Mamay Customs</a></li>
                    <li><a href="">MattPear</a></li>
                    <li><a href="">Mexanika Smoke</a></li>

                    <li><a href="">Maklaud</a></li>
                    <li><a href="">Shisha</a></li>
                    <li><a href="">Mamay Customs</a></li>
                    <li><a href="">MattPear</a></li>
                    <li><a href="">Mexanika Smoke</a></li>
                </ul>
            </div>
            
            <div class="catalog-product">
                <div class="product-slider__item">
                    <div class="product-slider__image">
                        <img src="<?php bloginfo('template_directory') ?>/assets/img/ugol/1.png" alt="">
                    </div>
                    <a href="" class="product-slider__name">
                        Pharaon Premium M1007
                    </a>
                    <div class="product-slider__price">
                        3780 руб
                        <span class="product-price-info">
                            <ul class="product-price__list">
                                <li>
                                    <span>3500 руб</span>
                                    При покупке товара на сумму от 30.000 рублей
                                </li>
                                <li>
                                    <span>3380 руб</span>
                                    При покупке товара на сумму от 100.000 рублей
                                </li>
                                <li>
                                    <span>2980 руб</span>
                                    При покупке товара на сумму от 200.000 рублей
                                </li>
                            </ul>
                        </span>
                    </div>
                    <div class="product-slider__bottom">
                        <div class="product-slider__counter product-counter">
                            <button type="button" class="product-counter__add">-</button>
                            <input class="product-counter__number" value="1" type="text">
                            <button type="button" class="product-counter__remove">+</button>
                        </div>
                        <button type="submit" class="btn btn-regular btn-orange circleflash">
                            <span class="btn__text">В корзину</span>
                            <span class="circle"></span>
                        </button>
                    </div>
                </div>

                <div class="product-slider__item">
                    <div class="product-slider__image">
                        <img src="<?php bloginfo('template_directory') ?>/assets/img/ugol/2.png" alt="">
                        <div class="product-badge product-best-price">
                            Лучшая <br> цена на <br> рынке
                        </div>
                    </div>
                    <a href="" class="product-slider__name">
                        SkySeven Vector Gray Light Green
                    </a>
                    <div class="product-slider__price">
                        5150 руб
                        <span class="product-price-info">
                            <ul class="product-price__list">
                                <li>
                                    <span>3500 руб</span>
                                    При покупке товара на сумму от 30.000 рублей
                                </li>
                                <li>
                                    <span>3380 руб</span>
                                    При покупке товара на сумму от 100.000 рублей
                                </li>
                                <li>
                                    <span>2980 руб</span>
                                    При покупке товара на сумму от 200.000 рублей
                                </li>
                            </ul>
                        </span>
                    </div>
                    <div class="product-slider__bottom">
                        <div class="product-slider__counter product-counter">
                            <button type="button" class="product-counter__add">-</button>
                            <input class="product-counter__number" value="1" type="text">
                            <button type="button" class="product-counter__remove">+</button>
                        </div>
                        <button type="submit" class="btn btn-regular btn-orange circleflash">
                            <span class="btn__text">В корзину</span>
                            <span class="circle"></span>
                        </button>
                    </div>
                </div>

                <div class="product-slider__item">
                    <div class="product-slider__image">
                        <img src="<?php bloginfo('template_directory') ?>/assets/img/ugol/3.png" alt="">
                    </div>
                    <a href="" class="product-slider__name">
                        Amy Deluxe SS 09 Stick Steel
                    </a>
                    <div class="product-slider__price">
                        9630 руб
                        <span class="product-price-info">
                            <ul class="product-price__list">
                                <li>
                                    <span>3500 руб</span>
                                    При покупке товара на сумму от 30.000 рублей
                                </li>
                                <li>
                                    <span>3380 руб</span>
                                    При покупке товара на сумму от 100.000 рублей
                                </li>
                                <li>
                                    <span>2980 руб</span>
                                    При покупке товара на сумму от 200.000 рублей
                                </li>
                            </ul>
                        </span>
                    </div>
                    <div class="product-slider__bottom">
                        <div class="product-slider__counter product-counter">
                            <button type="button" class="product-counter__add">-</button>
                            <input class="product-counter__number" value="1" type="text">
                            <button type="button" class="product-counter__remove">+</button>
                        </div>
                        <button type="submit" class="btn btn-regular btn-orange circleflash">
                            <span class="btn__text">В корзину</span>
                            <span class="circle"></span>
                        </button>
                    </div>
                </div>

                <div class="product-slider__item">
                    <div class="product-slider__image">
                        <img src="<?php bloginfo('template_directory') ?>/assets/img/ugol/4.png" alt="">
                        <div class="product-badge product-bestseller">
                            Хит <br>
                            продаж
                        </div>
                    </div>
                    <a href="" class="product-slider__name">
                        Amy 4-Star 410 Silver Red
                    </a>
                    <div class="product-slider__price">
                        4200 руб
                        <span class="product-price-info">
                            <ul class="product-price__list">
                                <li>
                                    <span>3500 руб</span>
                                    При покупке товара на сумму от 30.000 рублей
                                </li>
                                <li>
                                    <span>3380 руб</span>
                                    При покупке товара на сумму от 100.000 рублей
                                </li>
                                <li>
                                    <span>2980 руб</span>
                                    При покупке товара на сумму от 200.000 рублей
                                </li>
                            </ul>
                        </span>
                    </div>
                    <div class="product-slider__bottom">
                        <div class="product-slider__counter product-counter">
                            <button type="button" class="product-counter__add">-</button>
                            <input class="product-counter__number" value="1" type="text">
                            <button type="button" class="product-counter__remove">+</button>
                        </div>
                        <button type="submit" class="btn btn-regular btn-orange circleflash">
                            <span class="btn__text">В корзину</span>
                            <span class="circle"></span>
                        </button>
                    </div>
                </div>

                <div class="product-slider__item">
                    <div class="product-slider__image">
                        <img src="<?php bloginfo('template_directory') ?>/assets/img/ugol/5.png" alt="">
                    </div>
                    <a href="" class="product-slider__name">
                        AMY Deluxe 630 Black
                    </a>
                    <div class="product-slider__price">
                        5452 руб
                        <span class="product-price-info">
                            <ul class="product-price__list">
                                <li>
                                    <span>3500 руб</span>
                                    При покупке товара на сумму от 30.000 рублей
                                </li>
                                <li>
                                    <span>3380 руб</span>
                                    При покупке товара на сумму от 100.000 рублей
                                </li>
                                <li>
                                    <span>2980 руб</span>
                                    При покупке товара на сумму от 200.000 рублей
                                </li>
                            </ul>
                        </span>
                    </div>
                    <div class="product-slider__bottom">
                        <div class="product-slider__counter product-counter">
                            <button type="button" class="product-counter__add">-</button>
                            <input class="product-counter__number" value="1" type="text">
                            <button type="button" class="product-counter__remove">+</button>
                        </div>
                        <button type="submit" class="btn btn-regular btn-orange circleflash">
                            <span class="btn__text">В корзину</span>
                            <span class="circle"></span>
                        </button>
                    </div>
                </div>

                <div class="product-slider__item">
                    <div class="product-slider__image">
                        <img src="<?php bloginfo('template_directory') ?>/assets/img/ugol/6.png" alt="">
                    </div>
                    <a href="" class="product-slider__name">
                        Avanti Avantik Blue
                    </a>
                    <div class="product-slider__price">
                        3500 руб
                        <span class="product-price-info">
                            <ul class="product-price__list">
                                <li>
                                    <span>3500 руб</span>
                                    При покупке товара на сумму от 30.000 рублей
                                </li>
                                <li>
                                    <span>3380 руб</span>
                                    При покупке товара на сумму от 100.000 рублей
                                </li>
                                <li>
                                    <span>2980 руб</span>
                                    При покупке товара на сумму от 200.000 рублей
                                </li>
                            </ul>
                        </span>
                    </div>
                    <div class="product-slider__bottom">
                        <div class="product-slider__counter product-counter">
                            <button type="button" class="product-counter__add">-</button>
                            <input class="product-counter__number" value="1" type="text">
                            <button type="button" class="product-counter__remove">+</button>
                        </div>
                        <button type="submit" class="btn btn-regular btn-orange circleflash">
                            <span class="btn__text">В корзину</span>
                            <span class="circle"></span>
                        </button>
                    </div>
                </div>

                <div class="product-slider__item">
                    <div class="product-slider__image">
                        <img src="<?php bloginfo('template_directory') ?>/assets/img/ugol/7.png" alt="">
                        <div class="product-badge product-gift">
                            При <br>
                            покупке <br>
                            подарок
                        </div>
                    </div>
                    <a href="" class="product-slider__name">
                        CWP GRAVITY
                    </a>
                    <div class="product-slider__price">
                        26300 руб
                        <span class="product-price-info">
                            <ul class="product-price__list">
                                <li>
                                    <span>3500 руб</span>
                                    При покупке товара на сумму от 30.000 рублей
                                </li>
                                <li>
                                    <span>3380 руб</span>
                                    При покупке товара на сумму от 100.000 рублей
                                </li>
                                <li>
                                    <span>2980 руб</span>
                                    При покупке товара на сумму от 200.000 рублей
                                </li>
                            </ul>
                        </span>
                    </div>
                    <div class="product-slider__bottom">
                        <div class="product-slider__counter product-counter">
                            <button type="button" class="product-counter__add">-</button>
                            <input class="product-counter__number" value="1" type="text">
                            <button type="button" class="product-counter__remove">+</button>
                        </div>
                        <button type="submit" class="btn btn-regular btn-orange circleflash">
                            <span class="btn__text">В корзину</span>
                            <span class="circle"></span>
                        </button>
                    </div>
                </div>

                <div class="product-slider__item">
                    <div class="product-slider__image">
                        <img src="<?php bloginfo('template_directory') ?>/assets/img/ugol/8.png" alt="">
                    </div>
                    <a href="" class="product-slider__name">
                        Wookah Black-Blue Mill
                    </a>
                    <div class="product-slider__price">
                        32000 руб
                        <span class="product-price-info">
                            <ul class="product-price__list">
                                <li>
                                    <span>3500 руб</span>
                                    При покупке товара на сумму от 30.000 рублей
                                </li>
                                <li>
                                    <span>3380 руб</span>
                                    При покупке товара на сумму от 100.000 рублей
                                </li>
                                <li>
                                    <span>2980 руб</span>
                                    При покупке товара на сумму от 200.000 рублей
                                </li>
                            </ul>
                        </span>
                    </div>
                    <div class="product-slider__bottom">
                        <div class="product-slider__counter product-counter">
                            <button type="button" class="product-counter__add">-</button>
                            <input class="product-counter__number" value="1" type="text">
                            <button type="button" class="product-counter__remove">+</button>
                        </div>
                        <button type="submit" class="btn btn-regular btn-orange circleflash">
                            <span class="btn__text">В корзину</span>
                            <span class="circle"></span>
                        </button>
                    </div>
                </div>


                <div class="product-slider__item">
                    <div class="product-slider__image">
                        <img src="<?php bloginfo('template_directory') ?>/assets/img/ugol/1.png" alt="">
                    </div>
                    <a href="" class="product-slider__name">
                        Pharaon Premium M1007
                    </a>
                    <div class="product-slider__price">
                        3780 руб
                        <span class="product-price-info">
                            <ul class="product-price__list">
                                <li>
                                    <span>3500 руб</span>
                                    При покупке товара на сумму от 30.000 рублей
                                </li>
                                <li>
                                    <span>3380 руб</span>
                                    При покупке товара на сумму от 100.000 рублей
                                </li>
                                <li>
                                    <span>2980 руб</span>
                                    При покупке товара на сумму от 200.000 рублей
                                </li>
                            </ul>
                        </span>
                    </div>
                    <div class="product-slider__bottom">
                        <div class="product-slider__counter product-counter">
                            <button type="button" class="product-counter__add">-</button>
                            <input class="product-counter__number" value="1" type="text">
                            <button type="button" class="product-counter__remove">+</button>
                        </div>
                        <button type="submit" class="btn btn-regular btn-orange circleflash">
                            <span class="btn__text">В корзину</span>
                            <span class="circle"></span>
                        </button>
                    </div>
                </div>

                <div class="product-slider__item">
                    <div class="product-slider__image">
                        <img src="<?php bloginfo('template_directory') ?>/assets/img/ugol/2.png" alt="">
                        <div class="product-badge product-best-price">
                            Лучшая <br> цена на <br> рынке
                        </div>
                    </div>
                    <a href="" class="product-slider__name">
                        SkySeven Vector Gray Light Green
                    </a>
                    <div class="product-slider__price">
                        5150 руб
                        <span class="product-price-info">
                            <ul class="product-price__list">
                                <li>
                                    <span>3500 руб</span>
                                    При покупке товара на сумму от 30.000 рублей
                                </li>
                                <li>
                                    <span>3380 руб</span>
                                    При покупке товара на сумму от 100.000 рублей
                                </li>
                                <li>
                                    <span>2980 руб</span>
                                    При покупке товара на сумму от 200.000 рублей
                                </li>
                            </ul>
                        </span>
                    </div>
                    <div class="product-slider__bottom">
                        <div class="product-slider__counter product-counter">
                            <button type="button" class="product-counter__add">-</button>
                            <input class="product-counter__number" value="1" type="text">
                            <button type="button" class="product-counter__remove">+</button>
                        </div>
                        <button type="submit" class="btn btn-regular btn-orange circleflash">
                            <span class="btn__text">В корзину</span>
                            <span class="circle"></span>
                        </button>
                    </div>
                </div>

                <div class="product-slider__item">
                    <div class="product-slider__image">
                        <img src="<?php bloginfo('template_directory') ?>/assets/img/ugol/3.png" alt="">
                    </div>
                    <a href="" class="product-slider__name">
                        Amy Deluxe SS 09 Stick Steel
                    </a>
                    <div class="product-slider__price">
                        9630 руб
                        <span class="product-price-info">
                            <ul class="product-price__list">
                                <li>
                                    <span>3500 руб</span>
                                    При покупке товара на сумму от 30.000 рублей
                                </li>
                                <li>
                                    <span>3380 руб</span>
                                    При покупке товара на сумму от 100.000 рублей
                                </li>
                                <li>
                                    <span>2980 руб</span>
                                    При покупке товара на сумму от 200.000 рублей
                                </li>
                            </ul>
                        </span>
                    </div>
                    <div class="product-slider__bottom">
                        <div class="product-slider__counter product-counter">
                            <button type="button" class="product-counter__add">-</button>
                            <input class="product-counter__number" value="1" type="text">
                            <button type="button" class="product-counter__remove">+</button>
                        </div>
                        <button type="submit" class="btn btn-regular btn-orange circleflash">
                            <span class="btn__text">В корзину</span>
                            <span class="circle"></span>
                        </button>
                    </div>
                </div>

                <div class="product-slider__item">
                    <div class="product-slider__image">
                        <img src="<?php bloginfo('template_directory') ?>/assets/img/ugol/4.png" alt="">
                        <div class="product-badge product-bestseller">
                            Хит <br>
                            продаж
                        </div>
                    </div>
                    <a href="" class="product-slider__name">
                        Amy 4-Star 410 Silver Red
                    </a>
                    <div class="product-slider__price">
                        4200 руб
                        <span class="product-price-info">
                            <ul class="product-price__list">
                                <li>
                                    <span>3500 руб</span>
                                    При покупке товара на сумму от 30.000 рублей
                                </li>
                                <li>
                                    <span>3380 руб</span>
                                    При покупке товара на сумму от 100.000 рублей
                                </li>
                                <li>
                                    <span>2980 руб</span>
                                    При покупке товара на сумму от 200.000 рублей
                                </li>
                            </ul>
                        </span>
                    </div>
                    <div class="product-slider__bottom">
                        <div class="product-slider__counter product-counter">
                            <button type="button" class="product-counter__add">-</button>
                            <input class="product-counter__number" value="1" type="text">
                            <button type="button" class="product-counter__remove">+</button>
                        </div>
                        <button type="submit" class="btn btn-regular btn-orange circleflash">
                            <span class="btn__text">В корзину</span>
                            <span class="circle"></span>
                        </button>
                    </div>
                </div>

                <div class="product-slider__item">
                    <div class="product-slider__image">
                        <img src="<?php bloginfo('template_directory') ?>/assets/img/ugol/5.png" alt="">
                    </div>
                    <a href="" class="product-slider__name">
                        AMY Deluxe 630 Black
                    </a>
                    <div class="product-slider__price">
                        5452 руб
                        <span class="product-price-info">
                            <ul class="product-price__list">
                                <li>
                                    <span>3500 руб</span>
                                    При покупке товара на сумму от 30.000 рублей
                                </li>
                                <li>
                                    <span>3380 руб</span>
                                    При покупке товара на сумму от 100.000 рублей
                                </li>
                                <li>
                                    <span>2980 руб</span>
                                    При покупке товара на сумму от 200.000 рублей
                                </li>
                            </ul>
                        </span>
                    </div>
                    <div class="product-slider__bottom">
                        <div class="product-slider__counter product-counter">
                            <button type="button" class="product-counter__add">-</button>
                            <input class="product-counter__number" value="1" type="text">
                            <button type="button" class="product-counter__remove">+</button>
                        </div>
                        <button type="submit" class="btn btn-regular btn-orange circleflash">
                            <span class="btn__text">В корзину</span>
                            <span class="circle"></span>
                        </button>
                    </div>
                </div>

                <div class="product-slider__item">
                    <div class="product-slider__image">
                        <img src="<?php bloginfo('template_directory') ?>/assets/img/ugol/6.png" alt="">
                    </div>
                    <a href="" class="product-slider__name">
                        Avanti Avantik Blue
                    </a>
                    <div class="product-slider__price">
                        3500 руб
                        <span class="product-price-info">
                            <ul class="product-price__list">
                                <li>
                                    <span>3500 руб</span>
                                    При покупке товара на сумму от 30.000 рублей
                                </li>
                                <li>
                                    <span>3380 руб</span>
                                    При покупке товара на сумму от 100.000 рублей
                                </li>
                                <li>
                                    <span>2980 руб</span>
                                    При покупке товара на сумму от 200.000 рублей
                                </li>
                            </ul>
                        </span>
                    </div>
                    <div class="product-slider__bottom">
                        <div class="product-slider__counter product-counter">
                            <button type="button" class="product-counter__add">-</button>
                            <input class="product-counter__number" value="1" type="text">
                            <button type="button" class="product-counter__remove">+</button>
                        </div>
                        <button type="submit" class="btn btn-regular btn-orange circleflash">
                            <span class="btn__text">В корзину</span>
                            <span class="circle"></span>
                        </button>
                    </div>
                </div>

                <div class="product-slider__item">
                    <div class="product-slider__image">
                        <img src="<?php bloginfo('template_directory') ?>/assets/img/ugol/7.png" alt="">
                    </div>
                    <a href="" class="product-slider__name">
                        CWP GRAVITY
                    </a>
                    <div class="product-slider__price">
                        26300 руб
                        <span class="product-price-info">
                            <ul class="product-price__list">
                                <li>
                                    <span>3500 руб</span>
                                    При покупке товара на сумму от 30.000 рублей
                                </li>
                                <li>
                                    <span>3380 руб</span>
                                    При покупке товара на сумму от 100.000 рублей
                                </li>
                                <li>
                                    <span>2980 руб</span>
                                    При покупке товара на сумму от 200.000 рублей
                                </li>
                            </ul>
                        </span>
                    </div>
                    <div class="product-slider__bottom">
                        <div class="product-slider__counter product-counter">
                            <button type="button" class="product-counter__add">-</button>
                            <input class="product-counter__number" value="1" type="text">
                            <button type="button" class="product-counter__remove">+</button>
                        </div>
                        <button type="submit" class="btn btn-regular btn-orange circleflash">
                            <span class="btn__text">В корзину</span>
                            <span class="circle"></span>
                        </button>
                    </div>
                </div>

                <div class="product-slider__item">
                    <div class="product-slider__image">
                        <img src="<?php bloginfo('template_directory') ?>/assets/img/ugol/8.png" alt="">
                    </div>
                    <a href="" class="product-slider__name">
                        Wookah Black-Blue Mill
                    </a>
                    <div class="product-slider__price">
                        32000 руб
                        <span class="product-price-info">
                            <ul class="product-price__list">
                                <li>
                                    <span>3500 руб</span>
                                    При покупке товара на сумму от 30.000 рублей
                                </li>
                                <li>
                                    <span>3380 руб</span>
                                    При покупке товара на сумму от 100.000 рублей
                                </li>
                                <li>
                                    <span>2980 руб</span>
                                    При покупке товара на сумму от 200.000 рублей
                                </li>
                            </ul>
                        </span>
                    </div>
                    <div class="product-slider__bottom">
                        <div class="product-slider__counter product-counter">
                            <button type="button" class="product-counter__add">-</button>
                            <input class="product-counter__number" value="1" type="text">
                            <button type="button" class="product-counter__remove">+</button>
                        </div>
                        <button type="submit" class="btn btn-regular btn-orange circleflash">
                            <span class="btn__text">В корзину</span>
                            <span class="circle"></span>
                        </button>
                    </div>
                </div>

                <div class="more-product">
                    <button type="submit" class="btn btn-regular btn-orange circleflash">
                        <span class="btn__text">Еще кальяны</span>
                        <span class="circle"></span>
                    </button>
                </div>
            </div>
        </section>

    </main>
    <!-- /.main -->
<?php
get_footer();