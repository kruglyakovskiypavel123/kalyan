<?php
/**
 * Template name: Entering page
 */

get_header('entering');
?>
    <div class="entering-modal">
        <div class="entering-modal__block">
            <div class="entering-modal__title">Подтвердите, что вам исполнилось 18 лет и более</div>

            <div class="entering-modal__text">
                Федеральный закон Российской Федерации от 23 февраля 2013 г. N 15-ФЗ
                "Об охране здоровья граждан от воздействия окружающего табачного
                дыма и последствий потребления табака".Для дальнейшего просмотра
                сайта сигарного магазина, Вам необходимо подтвердить
                свое совершеннолетие.
            </div>
            <div class="entering-modal__buttons">
                <a href="index.html" class="btn btn-big btn-orange circleflash">
                    <span class="btn__text">Мне 18</span>
                    <span class="circle"></span>
                </a>
                <button type="submit" class="btn btn-big btn-orange circleflash">
                    <span class="btn__text">Нет 18-ти</span>
                    <span class="circle"></span>
                </button>
            </div>
        </div>
    </div>
<?php
get_footer('entering');