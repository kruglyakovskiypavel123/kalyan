<!DOCTYPE html>
<html lang="ru">
<head>
    <?php wp_head(); ?>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="<?php bloginfo('template_directory') ?>/assets/css/custom.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory') ?>/assets/css/swiper-bundle.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_directory') ?>/assets/css/nice-select.css">

</head>
<body>

    <header class="header">
        <a href="/" class="logo"><img src="<?php bloginfo('template_directory') ?>/assets/img/logotip.png" alt="Логотип"></a>

        <div class="search">
            <input class="search__input" type="text" placeholder="Поиск товаров по сайту">
            <button type="button" class="search-btn circleflash" onclick="window.location.href='/search-page/'"><span class="circle"></span></button>
        </div>

        <div class="feedback">
            <a href="tel:+79269185475" class="phone">
                <i class="i-phone"></i>
            </a>
            <div class="feedback__text">
                <a href="tel:+79269185475" class="phone-number">
                    +7 926 918-54-75
                </a>
                <button type="button" class="feedback__btn modal-btn" data-modal-button="feedback">Обратная связь</button>
            </div>
        </div>
        
        <div class="user-block">
            <button href="" class="user-avatar modal-btn" data-modal-button="login">
                <img src="<?php bloginfo('template_directory') ?>/assets/img/icon/header/user.png" alt="">
            </button>
            <a href="/register-page/" class="signup" target="_blank">
                Регистрация
            </a>
            <button type="button" class="login modal-btn" data-modal-button="login">
                Войти
            </button>
        </div>

        <div class="cart cart-empty">
            <a href="/cart/" class="cart__link">
                <img class="cart__icon" src="<?php bloginfo('template_directory') ?>/assets/img/icon/header/cart-empty.png" alt="">
                <div class="cart__price">
                    В корзине пусто
                </div>
                <div class="cart__number">(0)</div>
            </a>
        </div>

        <button class="menu-btn"></button>
    </header>
    
	<div class="left-menu">
        <ul>
            <li>
                <a href="/catalog-tabak-page/">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icon/left-menu/tabak.png" alt="">
                </a>
            </li>
            <li>
                <a href="/catalog-kalyan-page/">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icon/left-menu/kalyan.png" alt="">
                </a>
            </li>
            <li>
                <a href="/catalog-ugol-page/">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icon/left-menu/ugol.png" alt="">
                </a>
            </li>
            <li>
                <a href="">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icon/left-menu/komplect.png" alt="">
                </a>
            </li>
        </ul>
    </div>
    <!-- /.left-menu -->

    <div class="right-menu">
        <ul>
            <li>
                <a href="/about-page/">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icon/right-menu/about.png" alt="">
                </a>
            </li>
            <li>
                <a href="/contact-page/">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icon/right-menu/contacts.png" alt="">
                </a>
            </li>
            <li>
                <a href="/">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icon/right-menu/price.png" alt="">
                </a>
            </li>
            <li>
                <a href="/checkout-step-one-page/">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icon/right-menu/deliverys.png" alt="">
                </a>
            </li>
        </ul>
    </div>
    <!-- /.right-menu -->

    <button type="button" class="scroll-top"></button>
    <!-- /.scroll-top -->