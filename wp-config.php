<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'KalyanDataBase' );

/** MySQL database username */
define( 'DB_USER', 'KalyanDataBase' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '|^!ShUr-7N&6J-)&?v[})B(fKPc:5r FzzIBl{y?-O#Cc]=&e61tG{V5l:[JuDT@' );
define( 'SECURE_AUTH_KEY',  'cUiz0]-Y46jHo2Io{D;s2NU^HPB36wB^eUz4(]wTwnB(U|Z-8K~S+8}$2`oO?LhI' );
define( 'LOGGED_IN_KEY',    'al/8YrKX^::(Vznrg,n|#L@a/ u}9^5HS_N8=E.A. .:O,y$}GW>xXj6fk-wO{C?' );
define( 'NONCE_KEY',        'Oh_!xG)`U-YK#t/rOzuXQsx/#arZls~V5=jFs6>:Mtv.9ZYsQ@dTirY>hF|^$FW0' );
define( 'AUTH_SALT',        'oThNt:&%1o$J3a5[;kb&?1{ujUY6ie2awd+BlY:Vk$)!00/$1,Lfv=<4Sbby!O/U' );
define( 'SECURE_AUTH_SALT', ' WPy((AAf{!Y{a]*$fK4]lZZGL7?LlT%~FBnsI(V?-|#(fuu0D8V?<u.oa~,0/n2' );
define( 'LOGGED_IN_SALT',   '`#x&.z}11Mej&xb|m`1Y`lw?RUw:_wU83<a~gtn(upmm0!x$SWS8/V,F3:-T*r/u' );
define( 'NONCE_SALT',       'V:4k mP!;gy~8Ha6+ew$_fS)oURLhxki ^cMr2Q`EqdK{uf7T~MVkON8XFD@-g.v' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
